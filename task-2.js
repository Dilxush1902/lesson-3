// 1-Write a program that is divisible by 3 and 5. Start***
    let initialState = 45   ;
    let sum = 0;
    for(let i=1;i<=initialState; i++) {
        if(i%3==0 && i%5==0) {
            console.log(i);
        }
    }

// 1-Write a program that is divisible by 3 and 5. End***

/////////////////////////////////////////////////////////////////////////////////////////////////////

// 2-Write a program that checks whether given number odd or even. Start***
let num=33;
if(num%2==0){
    console.log(num + " is an Even Number");
}
else{
    console.log(num + " is an Odd Number");
}
// 2-Write a program that checks whether given number odd or even. End***

////////////////////////////////////////////////////////////////////////////////////////////////////

//3-Write a program that get array as argument and sorts it by order // [1,2,3,4...]. Start***
let numbers = [0, 15 , 2, 45, 10, 20, 30,66,333 ];
    numbers.sort( function( a , b){
        if(a > b) return 1;
        if(a < b) return -1;
        return 0;
    });

console.log(numbers);
//3-Write a program that get array as argument and sorts it by order // [1,2,3,4...]. End***

/////////////////////////////////////////////////////////////////////////////////////////////////////

//4-Write a program that return unique set of array: Start***
    Input:
    var arr =[{test: ['a', 'b', 'c', 'd']},{test: ['a', 'b', 'c']},{test: ['a', 'd']},{test: ['a', 'b', 'k', 'e', 'e']}];  
    // Output: [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }
    
    let newArr = []
    for(let key of arr) {
        for(let keyTest of key.test) {
            newArr.push(keyTest)
        }
    }
    var unique = newArr.filter(onlyUnique);
    console.log(unique);
//4-Write a program that return unique set of array: End***

//////////////////////////////////////////////////////////////////////////////////////////////////////

//5-Write a program that compares two objects by its values and keys, return true if object values are same otherwise return false. Start****

    const matches = (obj, source) =>
        Object.keys(source).every(key => obj.hasOwnProperty(key) && obj[key] === source[key]);
        
        console.log(matches({ age: 25, hair: 'long', beard: true }, { hair: 'long', beard: true })); // true
        console.log(matches({ hair: 'long', beard: true }, { age: 25, hair: 'long', beard: true })); // false
        console.log(matches({ hair: 'long', beard: true }, { age: 26, hair: 'long', beard: true })); // false

//5-Write a program that compares two objects by its values and keys, return true if object values are same otherwise return false. End****

